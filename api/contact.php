<?php
session_start();
require_once '../libs/phpmailer/PHPMailerAutoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App;

$app->post('/enviarTeste', function (Request $request, Response $response) {
    $fields=[
        'nome'=>$request->getParam('Nome'),
        'email'=>$request->getParam('eMail'),
        'mensagem'=>$request->getParam('Mensagem')
    ];
    $m=new PHPMailer;
    $m->isSMTP();
    $m->SMTPDebug = 1;
    $m->SMTPAuth=true;
    $m->Host='smtp.zoho.com';
    $m->Username='sac@evoxtech.com.br';
    $m->Password='12345678';
    $m->SMTPSecure='ssl';
    $m->Port=465;

    $m->isHTML(true);
    $m->Subject ='E-mail eviado pelo site';
    $m->Body='De:'.$fields['nome'].'('.$fields['email'].')<p>Mensagem: <br>'.$fields['mensagem'].'</p>';

    $m->SetFrom('sac@evoxtech.com.br','SAC');
    $m->AddAddress('ti5@evoxtech.com.br','SAC eWorld');
    if ($m->send()) {
        echo "deu certo";
        die();
    }
});
$app->post('/enviarMail/{destMail}', function (Request $request, Response $response) {
    $fields=[
        'nome'=>$request->getParam('Nome'),
        'email'=>$request->getParam('eMail'),
        'mensagem'=>$request->getParam('Mensagem')
    ];
    $destMail = $request->getAttribute('destMail');
    $m=new PHPMailer;
    $m->isSMTP();
    $m->SMTPDebug = 1;
    $m->SMTPAuth=true;
    $m->Host='smtp.zoho.com';
    $m->Username='sac@evoxtech.com.br';
    $m->Password='12345678';
    $m->SMTPSecure='ssl';
    $m->Port=465;

    $m->isHTML(true);
    $m->Subject ='E-mail eviado pelo site';
    $m->Body='De:'.$fields['nome'].'('.$fields['email'].')<p>Mensagem: <br>'.$fields['mensagem'].'</p>';

    $m->SetFrom('sac@evoxtech.com.br','SAC');
    $m->AddAddress($destMail,'SAC eWorld');
    if ($m->send()) {
        echo "deu certo";
        die();
    }
});

$app->run();
