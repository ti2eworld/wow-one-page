var app = angular.module('angularGeral', ['ngAnimate','duScroll'])
var local_include = ["include/concta.html","include/dream.html","include/ew.html","include/vox.html","include/wow.html"];
function increment(num){
  num++;
  if(num > 3){
    num = 0;
  }
  return num;
}
function corta_string(str,fim){
  if(fim > str.length) return str;
  var res1 = str.slice(0, fim);
  var res2 = str.slice(fim, str.length);
  res2 = res2.split(" ");
  return (res1 + res2[0]+"...");
}
app.controller('Ctrl_Somos_eWorld', function($scope, $http, $interval) {
  var opcao = 0;
  var parar;
  var bool = true;
	$scope.myfuncition =  function(qOpicao){
		for (var i = 3; i >= 0; i--) $scope.myVar[i] = "";
    bool = false;
		$scope.myVar[qOpicao] = "my-class";
    $scope.troca_var = local_include[qOpicao];
	}
  $scope.inicilizacaofuncao = function(){
    $scope.myVar[0] = "my-class";
    $scope.troca_var = local_include[0];
    parar = $interval(function(){
      if (bool) {
        opcao = increment(opcao);
        var temp = opcao;
        for (var i = 3; i >= 0; i--) $scope.myVar[i] = "";
        $scope.myVar[temp] = "my-class";
        $scope.troca_var = local_include[temp];
      }else{
        $interval.cancel(parar);
      }
    }, 3000);
  }
});
app.controller('MyCtrl', function($scope, $document){
    $scope.toTheTop = function() {
      $document.scrollTopAnimated(0, 800);
    }
  }
).value('duScrollOffset', 68);

app.controller('Ctrl_facebook', function($scope, $http) {
  $http.get('https://graph.facebook.com/me?fields=feed{picture,link,message}&&access_token=EAAZAdNdQrLZAoBAOJQSZABrTSEC66dEcp1ZCoFdjYGTukGrj2aZB4W2ZADyxQ3Gqtsoe3ukyPDmzxYda537fZB5kS85WjbXPkLGovmMZCDHn8V5V1t0Kfe2Nsf97NuAVCBZAvf1IxJJcBNpuXuAkEszzgzDzFy5WChUEZD').
    then(function(response) {
      $scope.facebook = response.data.feed.data;
      $scope.facebook.forEach(function(item){
       if(typeof item.message !== "undefined"){
          item.minMesagem = corta_string(item.message,220);
        };
      });
  });
});
app.controller('Ctrl_email', function($scope,$http,$rootElement,$location){
  $scope.enviarMail = function(){
    if(!$scope.user||!$scope.user.Nome||!$scope.user.eMail||!$scope.user.Mensagem) $scope.alerta = "Preencha todos os campos";
    else {
      var str = $location.absUrl();
      var indice = str.lastIndexOf("index.php");
      if( indice > -1) str = str.slice(0, indice);
      str = str + "api/contact.php/enviarMail/sac@eworld.com.br";
      $http.post(str, $scope.user).then(function(response){
        $scope.user.Nome = "";
        $scope.user.eMail = "";
        $scope.user.Mensagem = "";
        $scope.foi_mail = true;
      },function(response){
        $scope.alerta = "Ocorreu um erro <br>"+ response;
      });
    }
    
  }
});

